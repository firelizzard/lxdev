package main

import (
	"flag"
	"fmt"
	"log"
	"strings"
)

const root = "/sys/devices"
const offset = len(root)

var skipStr = flag.String("skip", "", "csv list of directories to skip")
var printFiles = flag.Bool("files", false, "print files")
var printAll = flag.Bool("all", false, "include directories that do not specify a subsystem or driver")

var skip []string

func main() {
	flag.Parse()
	skip = strings.Split(*skipStr, ",")

	var send func(dev *Device)
	ch := make(chan *Device)
	send = func(dev *Device) {
		for _, skip := range skip {
			if strings.Contains(dev.Path, skip) {
				return
			}
		}

		ch <- dev
		for _, dev := range dev.Children {
			send(dev)
		}
	}

	root, err := ReadDevice(root)
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		send(root)
		close(ch)
	}()

	for dev := range ch {
		if dev.IsPlainContainer() {
			continue
		}

		if !*printAll && !*printFiles && dev.Subsystem == nil && dev.Driver == nil {
			// boring, skip
			continue
		}

		if dev.Subsystem != nil {
			fmt.Printf("[%s:%s] ", dev.Subsystem.Kind, dev.Subsystem.Name)
		}

		if dev.Driver != nil {
			fmt.Printf("{%s:%s", dev.Driver.Kind, dev.Driver.Name)
			if dev.Driver.Module != nil {
				fmt.Printf(" from %s", dev.Driver.Module.Name)
			}
			fmt.Print("} ")
		}

		fmt.Println(dev.Path[offset+1:])

		if *printFiles {
			for _, file := range dev.Files {
				fmt.Println("  " + file)
			}
		}

		for _, link := range dev.Links {
			fmt.Printf("  %s -> %s\n", link.Name, link.Abs)
		}

		fmt.Println()
	}
}
