module gitlab.com/firelizzard/lxdev

go 1.16

require (
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68
	golang.org/x/term v0.0.0-20210503060354-a79de5458b56
)
