package main

import (
	"errors"
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/sys/unix"
)

type Device struct {
	Path      string
	Subsystem *Subsystem
	Driver    *Driver
	Links     []*Symlink
	Files     []string
	Children  []*Device
}

type Symlink struct {
	Name, Rel, Abs string
}

type Subsystem struct {
	Kind, Name, Path string
}

type Driver struct {
	Kind, Name, Path string
	Module           *Module
}

type Module struct {
	Name, Path string
}

func (d *Device) IsPlainContainer() bool {
	return true &&
		d.Subsystem == nil &&
		d.Driver == nil &&
		len(d.Files) == 0 &&
		len(d.Links) == 0
}

func ReadDevice(path string) (*Device, error) {
	dev := &Device{Path: path}

	items, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}

	if len(items) == 0 {
		return nil, nil
	}

	for _, item := range items {
		if item.Name() == "power" {
			continue
		}

		if item.IsDir() {
			child, err := ReadDevice(filepath.Join(path, item.Name()))
			if err != nil {
				return nil, err
			}

			if child == nil {
				dev.Files = append(dev.Files, fmt.Sprintf("%s/", item.Name()))
			} else {
				dev.Children = append(dev.Children, child)
			}
			continue
		}

		rel, abs, err := sysReadLink(filepath.Join(path, item.Name()))
		if err != nil {
			return nil, err
		} else if rel == "" {
			dev.Files = append(dev.Files, item.Name())
			continue
		}

		bits := strings.Split(abs, string(filepath.Separator))[2:]
		switch {
		case len(bits) == 2 && item.Name() == "subsystem":
			dev.Subsystem = &Subsystem{
				Kind: bits[0],
				Name: bits[1],
				Path: abs,
			}

		case len(bits) == 4 && item.Name() == "driver" && bits[0] == "bus" && bits[2] == "drivers":
			dev.Driver = &Driver{
				Kind: bits[1],
				Name: bits[3],
				Path: abs,
			}

			rel, abs, err := sysReadLink(filepath.Join(abs, "module"))
			if errors.Is(err, fs.ErrNotExist) {
				// ok
			} else if err != nil {
				return nil, err
			} else if rel != "" {
				dev.Driver.Module = &Module{
					Name: filepath.Base(abs),
					Path: abs,
				}
			}

		// case item.Name() == "device" && bits[0] == "devices":
		// 	dev.Device = filepath.Join(bits[1:]...)

		default:
			dev.Links = append(dev.Links, &Symlink{
				Name: item.Name(),
				Rel:  rel,
				Abs:  abs,
			})
		}
	}

	return dev, nil
}

func sysReadLink(path string) (rel, abs string, err error) {
	rel, err = os.Readlink(path)
	if errors.Is(err, unix.EINVAL) {
		return "", "", nil
	} else if err != nil {
		return "", "", err
	}

	if !filepath.IsAbs(rel) {
		abs, err = filepath.Abs(filepath.Join(path, "..", rel))
	}

	if !strings.HasPrefix(abs, "/sys") {
		return "", "", fmt.Errorf("link outside of /sys! %q", abs)
	}

	return rel, abs, err
}
